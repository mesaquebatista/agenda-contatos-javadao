package dao;

import conexao.FabricaConexao1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Contato;

/**
 *
 * @author familia
 */
public class ContatoDAO {
    public List<Contato> getAll() {
        String sql = "SELECT * FROM contatos";
        ResultSet rs = null;
        List<Contato> allContato = new ArrayList<>();
        try (Connection conn = FabricaConexao1.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {
            rs = ps.executeQuery();
            while (rs.next()) {
                Contato a = new Contato();
                a.setId(rs.getInt("id"));
                a.setNome(rs.getString("nome"));
                a.setApelido( rs.getString("apelido") );
                a.setEmail( rs.getString("email"));
                a.setDt_nascimento( rs.getString("dt_nascimento"));
                a.setTelefone( rs.getString("telefone"));
                a.setCategoria(rs.getString("categoria"));
                allContato.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            FabricaConexao1.fecharConexao(rs);
        }
        return allContato;
    }
    
    public Contato getById(Integer id) {
        if (id == null || id < 0) {
            throw new IllegalArgumentException();
        }
        String sql = "SELECT * FROM contatos WHERE id=?";
        ResultSet rs = null;
        Contato contato = null;
        try (Connection conn = FabricaConexao1.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, id);
            rs = ps.executeQuery();
            rs.next();
            contato = new Contato(rs.getInt("id"), rs.getString("nome"), rs.getString("apelido"), rs.getString("email"),rs.getString("dt_nascimento"), rs.getString("telefone"), rs.getString("categoria"));	
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            FabricaConexao1.fecharConexao(rs);
        }
        return contato;
    }
    
    public boolean create(Contato contato) {
        if (contato == null) {
            throw new IllegalArgumentException();
        }
        String sql = "INSERT INTO contatos (nome, apelido, email,dt_nascimento, telefone, categoria) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection conn = FabricaConexao1.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, contato.getNome() );
            ps.setString(2, contato.getApelido());
            ps.setString(3, contato.getEmail() );
            ps.setString(4, contato.getDt_nascimento());
            ps.setString(5, contato.getTelefone());
            ps.setString(6, contato.getCategoria());
            int linhasAfetadas = ps.executeUpdate();
            if (linhasAfetadas > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean update(Contato contato) {
        if (contato == null || contato.getId() <=0) {
            throw new IllegalArgumentException();
        }
        String sql = "UPDATE contatos SET nome = ?, email = ?, dt_nascimento=?, telefone=?, categoria=? WHERE id = ?";
        try (Connection conn = FabricaConexao1.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, contato.getNome() );
            ps.setString(2, contato.getEmail() );
            ps.setString(3, contato.getDt_nascimento());
            ps.setString(4, contato.getTelefone());
            ps.setString(5, contato.getCategoria());
            ps.setInt(6, contato.getId());
            int linhasAfetadas = ps.executeUpdate();
            if (linhasAfetadas > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean delete(Integer id) {
        if (id == null || id < 0) {
            throw new IllegalArgumentException();
        }
        String sql = "DELETE FROM contatos WHERE id = ?";
        try 
        (Connection conn = FabricaConexao1.getConexao();
            PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setInt(1, id);
            int linhasAfetadas = ps.executeUpdate();
            if (linhasAfetadas > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public List<Contato> getByName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException();
        }
        String sql = "SELECT * FROM contatos WHERE nome like ?";
        ResultSet rs = null;
        List<Contato> allContatos = new ArrayList<>();
        try (Connection conn = FabricaConexao1.getConexao(); PreparedStatement ps = conn.prepareStatement(sql)) {
            ps.setString(1, "%"+name+"%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Contato a = new Contato(rs.getInt("id"), rs.getString("nome"), rs.getString("apelido"), rs.getString("email"),rs.getString("dt_nascimento"), rs.getString("telefone"), rs.getString("categoria"));	
                allContatos.add(a);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            FabricaConexao1.fecharConexao(rs);
        }
        return allContatos;
    }    
}
